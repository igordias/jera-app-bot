# Jera app

This project aims for facilitating android project setups at Jera.

## What it does

1. Copies Android's app template for new projects
2. Creates all the necessary folders and transfer files for renaming the packages
3. Finds and replaces the old packages names by the new one on kotlin and java files
4. Sends the project with the renamed package to a repository.

## How to run it

Before executing the script:

- Provide the necessary permissions. Go to the jera-app-bot folder and execute the following command:

  `chmod 777 ./jera-app.sh`

## How to use it

1. Create a new project on GitLab and copy its remote origin
2. Run `./jera-app.sh`
   1. Insert your application class name (eg.: `TextFlightApplication`)
   2. Insert your new project's remote origin (eg.: `git@gitlab.com:igordias/my-new-project.git`)
   3. Insert a Package name (eg.: `com.jera.newapp`) 

